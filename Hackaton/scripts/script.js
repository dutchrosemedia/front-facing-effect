//
// Available modules include (this is not a complete list):
var Scene = require('Scene');
var Textures = require('Textures');
var Materials = require('Materials');
var FaceTracking = require('FaceTracking');
var Animation = require('Animation');
var Reactive = require('Reactive');
var HandTracking = require('HandTracking');
const Diagnostics = require('Diagnostics');

//Get reference to scene objects
var fd = Scene.root.child("Device").child("Camera").child("Focal Distance"); 
//var ambientAudio = Scene.root.child("Device").child("Camera").child("audiosource");
var camera = Scene.root.child("Device").child("Camera");
var handTracker = fd.child("handtracker");
var card = handTracker.child("card");
var lastLoggedValue;
var lastKnownZ;
var wasAboveThreshold = false;
var timeEffectShown;
var audiosources;
var playingAudioSource;

HandTracking.count.monitor().subscribe(function(e) {
    for(i = 0; i < e.newValue; i++)
    {
        HandTracking.hand(i).cameraTransform.y.monitor().subscribe(function(e){
            if(e.newValue != lastLoggedValue)
            {
                if(e.newValue < 0)
                {
                    wasAboveThreshold = false;
                    yPosToTransform(e.newValue);
                }
                else
                {
                    if(!wasAboveThreshold)
                    {
                        wasAboveThreshold = true;
                        timeEffectShown = new Date();
                        enableAudioSourceForPercentage(100);
                        handleCard();
                    }
                }
            }
        });
    }
})

var handleCard = function()
{
    Diagnostics.log("Card was shown");
}

var yPosToTransform = function (newYPos){
    var positive = (newYPos * -1) / 2;
    
    var percentage;

    if(positive > 10)
    {
        percentage = 100;
    }
    else
    {
        var percentage = positive * 10;
        var scalePercentage = 100 - percentage;
    }

    //ambientAudio.volume = 100;
    enableAudioSourceForPercentage(scalePercentage);
    //Diagnostics.log(ambientAudio);
    card.transform.rotationX = ((percentage / 100) * 1.8);
    card.transform.scaleX = (scalePercentage / 100);
    card.transform.scaleY = (scalePercentage / 100);
}

 var enableAudioSourceForPercentage = function(percentage) {

     var selectedAudioSource;
     //Diagnostics.log("Finding audiosource for " + percentage);
     var percentage = Math.floor( percentage );


     switch(true)
     {
        case percentage > 0 && percentage < 10:
            //selectedAudioSource = audiosources[0];       
            break;
        case percentage >= 10 && percentage < 20:
            //selectedAudioSource = audiosources[1];
            break;
        case percentage >= 20 && percentage < 30:
            //selectedAudioSource = audiosources[2];  
            break;
        case percentage >=  30 && percentage < 40:
            //selectedAudioSource = audiosources[3];   
            break;
        case percentage >= 40 && percentage < 50:
            //selectedAudioSource = audiosources[4];   
            break;
        case percentage >= 50 && percentage < 60:
            //electedAudioSource = audiosources[5];   
            break;
        case percentage >= 60 && percentage < 70:
            //selectedAudioSource = audiosources[6];
            break;
        case percentage >= 70 && percentage < 80:
            //selectedAudioSource = audiosources[7];
            break;
        case percentage >= 80 && percentage < 90:
            //selectedAudioSource = audiosources[8];
            break;
        case percentage > 90:
            selectedAudioSource = audiosources[8];
            break;
        default:
            Diagnostics.log("No audiosource for " + percentage);
            break;
     }

     audiosources.forEach(function(aS){
         if(aS == selectedAudioSource && percentage > 90)
         {
             if(aS != playingAudioSource)
             {
                playingAudioSource = selectedAudioSource;
                Diagnostics.log("playing for percentage " + percentage)
                aS.play();
             }             
         }
         else
         {
           // Diagnostics.log("Not showing audiosource");
            aS.stopAll();
         }
     })


 };


var setup = function()
{
    //Initial bindings

    audiosources = [    camera.find("audiosource20"),
                        camera.find("audiosource30"),
                        camera.find("audiosource40"),
                        camera.find("audiosource50"),
                        camera.find("audiosource60"),
                        camera.find("audiosource70"),
                        camera.find("audiosource80"),
                        camera.find("audiosource90"),
                        camera.find("audiosource100")];
}

setup();
